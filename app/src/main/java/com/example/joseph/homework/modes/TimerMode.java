package com.example.joseph.homework.modes;

import android.os.CountDownTimer;

import com.example.joseph.homework.modes.interfaces.IDataViewBridge;
import com.example.joseph.homework.modes.interfaces.IModeCallback;

/**
 * Created by Joseph on 11/7/16.
 */

public class TimerMode implements IDataViewBridge {

    private CountDownTimer mTimer;
    private IModeCallback mCallback;
    private long mTimeLeft;
    private boolean isStarted = false;

    @Override
    public void start() {
        if(mTimer != null) {
            isStarted = true;
            if(mCallback != null){
                mCallback.onStarted();
            }
            mTimer.start();
        }
    }

    @Override
    public void stop() {
        if(mTimer != null) {
            isStarted = false;
            if(mCallback != null){
                mCallback.onStopped();
            }
            mTimer.cancel();
        }
    }

    @Override
    public void set(long time) {
        time *= 1000;
        if(mTimer != null) mTimer.cancel();

        mTimer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) {
                mTimeLeft = l;
                if (mCallback != null) {
                    mCallback.onEvent("Seconds remaining: " + l / 1000);
                }
            }

            @Override
            public void onFinish() {
                mTimeLeft = 0;
                isStarted = false;
                if (mCallback != null) {
                    mCallback.onEvent("Done!");
                    mCallback.onStopped();
                }
            }
        };
    }

    @Override
    public String getStatusInfo() {
        return mTimeLeft == 0 ? "Done!" : ("Seconds remaining: " + mTimeLeft / 1000);
    }

    @Override
    public void setListener(IModeCallback callback) {
        mCallback = callback;
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }
}
