package com.example.joseph.homework.modes;

import android.os.Handler;
import android.os.Message;

import com.example.joseph.homework.modes.interfaces.IDataViewBridge;
import com.example.joseph.homework.modes.interfaces.IModeCallback;

/**
 * Created by Joseph on 11/7/16.
 */

public class StopWatchMode implements IDataViewBridge {

    private boolean isStarted = false;
    private IModeCallback mCallback;
    private Stopwatch mStopwatch = new Stopwatch();
    private static final int MESSAGE_START = 0;
    private static final int MESSAGE_UPDATE = 1;
    private static final int MESSAGE_STOP = 2;

    private static final int RATE = 1000;

    private Handler mHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MESSAGE_START){
                mStopwatch.start();
                sendEmptyMessage(MESSAGE_UPDATE);
            } else if (msg.what == MESSAGE_UPDATE) {
                if(mCallback != null){
                    mCallback.onEvent("Time elapsed in sec: " + mStopwatch.getElapsedTimeSecs());
                }
                sendEmptyMessageDelayed(MESSAGE_UPDATE, RATE);
            } else if (msg.what == MESSAGE_STOP) {
                removeMessages(MESSAGE_UPDATE);
                mStopwatch.stop();
                if(mCallback != null){
                    mCallback.onEvent("Time elapsed in sec: " + mStopwatch.getElapsedTimeSecs());
                    mCallback.onStopped();
                }
            }
        }
    };

    @Override
    public void start() {
        isStarted = true;
        if(mCallback != null){
            mCallback.onStarted();
        }
        mHandler.sendEmptyMessage(MESSAGE_START);
    }

    @Override
    public void stop() {
        isStarted = false;
        if(mCallback != null){
            mCallback.onStopped();
        }
        mHandler.sendEmptyMessage(MESSAGE_STOP);
    }

    @Override
    public void set(long time) {
        if(mCallback != null) {
            mCallback.alert("Stopwatch Mode");
        }
    }

    @Override
    public String getStatusInfo() {
        return "Time elapsed in sec: " + String.valueOf(mStopwatch.getElapsedTimeSecs());
    }

    @Override
    public void setListener(IModeCallback callback) {
        mCallback = callback;
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }
}
