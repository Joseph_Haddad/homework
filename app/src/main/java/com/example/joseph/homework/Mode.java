package com.example.joseph.homework;

/**
 * Created by Joseph on 11/7/16.
 */

public enum Mode {
    STOP_WATCH("Stopwatch"), TIMER("Timer"), GPS("GPS");

    private String convName;

    Mode(String s) {
        convName = s;
    }

    public String getConvenientName() {
        return convName;
    }
}
