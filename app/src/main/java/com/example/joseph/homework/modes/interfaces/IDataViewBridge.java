package com.example.joseph.homework.modes.interfaces;

/**
 * Created by Joseph on 11/7/16.
 */

public interface IDataViewBridge {
    void start();
    void stop();
    void set(long time);
    String getStatusInfo();
    void setListener(IModeCallback callback);

    boolean isStarted();
}
